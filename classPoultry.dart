import 'dart:io';
import 'classDinosaur.dart';

class poultryDinosaur extends Dinosaur {
  poultryDinosaur(super.name, super.numberLegs) {
    print("Poultry Dinosaur is born");
  }
  @override
  void hunt() {
    super.hunt();
  }

  @override
  void eat() {
    super.eat();
    print("$name is eating meat on the sky!!!");
  }

  void fly() {
    print("$name is flying on the sky");
  }
}
