import 'dart:io';

class Dinosaur {
  String name = "";
  int numberLegs = 0;

  Dinosaur(String name, int numberLegs) {
    this.name = name;
    this.numberLegs = numberLegs;
  }
  void walk() {
    print("$name is walk with $numberLegs legs");
  }

  void hunt() {
    print("$name is hunting!");
  }

  void run() {
    print("$name is running!");
  }
  void eat(){
  }

  String getName() {
    return name;
  }

  int getNumberLegs() {
    return numberLegs;
  }
}
