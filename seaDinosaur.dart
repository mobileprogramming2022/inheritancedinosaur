import 'dart:io';
import 'classDinosaur.dart';

class seaDinosaur extends Dinosaur {
  seaDinosaur(super.name, super.numberLegs) {
    print("Sea Dinosaur is born");
  }
  @override
  void eat() {
    print("$name is eating meat in deep sea.");
  }

  @override
  void hunt() {
    super.hunt();
  }

  void swim() {
    print("$name is swimming in deep sea.");
  }
}
