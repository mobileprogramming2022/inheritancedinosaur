import 'dart:io';
import 'classDinosaur.dart';

class Cornivorous extends Dinosaur {
  Cornivorous(super.name, super.numberLegs) {
    print("Cornivorous Dinosaur is born");
  }
  @override
  void walk() {
    super.walk();
    print("$name walk with $numberLegs Legs.");
  }

  @override
  void hunt() {
    super.hunt();
    print("$name is hunting in the world");
  }

  @override
  void eat() {
    print("$name is eating meat herbivore!!");
  }

  @override
  void run() {
    super.run();
    print("$name is running for hunting");
  }
}
