import 'dart:io';
import 'Cornivorous.dart';
import 'Herbivorous.dart';
import 'classDinosaur.dart';
import 'classPoultry.dart';
import 'seaDinosaur.dart';

void main(List<String> args) {
  Dinosaur dino = Dinosaur('DinoDino', 0);
  dino.walk();
  dino.hunt();
  print("--------------");

  Cornivorous corDino = Cornivorous("Tyrannosaurus", 2);
  corDino.walk();
  corDino.run();
  corDino.hunt();
  corDino.eat();
  print("--------------");

  Herbivorous herbiDino = Herbivorous("Apatosaurus", 4);
  herbiDino.walk();
  herbiDino.eat();
  print("--------------");

  poultryDinosaur poultry = poultryDinosaur("Pterodactyl", 2);
  poultry.fly();
  poultry.hunt();
  poultry.eat();
  print("--------------");

  seaDinosaur seaDino = seaDinosaur("Mosasaurus", 0);
  seaDino.swim();
  seaDino.hunt();
  seaDino.eat();
}
