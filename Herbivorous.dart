import 'dart:io';
import 'classDinosaur.dart';

class Herbivorous extends Dinosaur {
  Herbivorous(super.name, super.numberLegs) {
    print("Herbivorous Dinosaur is born");
  }
  @override
  void walk() {
    super.walk();
  }

  @override
  void eat() {
    print("$name is eating herbivorous");
  }
}
